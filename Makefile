# Makefile for static sites with jekyll on bitbucket.org
#
# Setting up
# ----------
#
# Create and clone repository on bitbucket for your static site.
# 
#     cd username.bitbucket.org
#     git branch source
#     git checkout source
#     rm -fr *
#     jekyll new .
# 
# then put this Makefile in the root and type
#
#     make
#
# You're done.

all: 
	git add .
	git commit -am "Source state `date +%Y-%m-%d-%D`" || true
	jekyll build
	rm -fr /tmp/_site/
	mv _site /tmp/
	git checkout master
	rm -fr *
	mv /tmp/_site/* .
	rm -fr /tmp/_site/
	git add .
	git commit -am "Generated `date +%Y-%m-%d-%D`" || true
	rm -fr *
	git checkout source
	git reset --hard HEAD

push:
	git push -u origin master
	git push -u origin source

test:  
	jekyll s
